# Optimo Keyboard Layout

The Optimo layout is a fork of the [bepo](http://bepo.fr) layout. 
It comes in three variants:

* [Optimo](#optimo): for ANSI keyboards (104 keys) 
* [Optima](#optima): for ISO keyboard (105 keys)
* [Optimo Plus](#optimo-Plus): for Planck Keyboard (40%)

## Features

* New position of M, W and Z
* Direct access to ´ (dead acute) and \` (dead grave)
* < and > in direct access, « and » in alt-gr
* \ { } \_ ; : ? ! at the same level (direct or shift) than a qwerty us keyboard

## Optimo

Layout for orthogonal or ANSI staggered keyboards of 104 keys

![Image](optimo/bepo-Optimo.png)

## Optima 

Layout for ISO keyboards of 105 keys 

![Image](optima/bepo-Optima.png)

## Optimo Plus

Optimo layout for the Planck keyboard (firmware [here](https://github.com/sgdjs/qmk_firmware/tree/optimo/keyboards/planck/keymaps/optimo))

**Special Feature**: optimized line of numbers and symbols

![Otimp](optimp/bepo-Optimp.png)

# Installation

The installation files are located in the `optima`, `optimo` and `optimp` folders.

### MacOS

Copy the keylayout file in the root or user library, like the `optim*/mac-copy.sh`
script does.

### Windows

Download and install the [Microsoft Keyboard Layout Creator](https://msdn.microsoft.com/en-us/globalization/keyboardlayouts)

* Open the file `bepo-Optim*B.klc` with MKLC
* Generate the DLL and install program
* Lauch setup.exe

### Linux

* Xkb: xorg keyboard layout
  * Copy the xkb file in `/usr/share/X11/xkb/symbols`, like the `optim*/linux-copy.sh` script does (needs sudo).
  * To change the layout, run in the terminal:
    ```
    setxkbmap froo  # or froa or frop
    ```
  * To allow to select for those layouts in the DE, make the following changes (needs sudo):
    *  In `/usr/share/X11/xkb/rules/evdev.xml`, add in the `layoutList` tag:
    ```xml
    <layout>
      <configItem>
        <name>froo</name>
        <shortDescription>Optimo</shortDescription>
        <description>Optimo</description>
        <languageList>
          <iso639Id>fra</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
    <layout>
      <configItem>
        <name>froa</name>
        <shortDescription>Optima</shortDescription>
        <description>Optima</description>
        <languageList>
          <iso639Id>fra</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
    <layout>
      <configItem>
        <name>frop</name>
        <shortDescription>Optimp</shortDescription>
        <description>Optimo Plus</description>
        <languageList>
          <iso639Id>fra</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
    ```
    * In `/usr/share/X11/xkb/rules/evdev.lst`, add (for example after "fr French"):
    ```
    froo   French (Optimo)
    froa   French (Optima)
    frop   French (Optimo Plus)
    ```
* Kbd: for some distro’s console
  * Copy the map file in `/usr/share/kbd/keymaps`, like the `optim*/linux-copy.sh` script does (needs sudo).
  * To make the change persistent put the following line with the desired layout in `/etc/vconsole.conf`
  ```
  KEYMAP=optimo
  ```

# More

* The `bepo/*` branches are set to track https://git.tuxfamily.org/dvorak/pilotes.git and https://gitlab.com/bepo/pilotes
* Instructions for the tools (French link): http://bepo.fr/wiki/ConfigGenerator
* About the same in French [here](http://bepo.fr/wiki/Utilisateur:Sgdjs).
* The Adjust layer is obtained with Raise and Lower at the same time. It has a few features.
* Underscore is not on AltGr + Space because:
  * Used for something else on a Poker3 keyboard 
  * Microsoft Keyboard Layout wouldn’t work
  * SQL Mgmt studio and other broken shortcuts